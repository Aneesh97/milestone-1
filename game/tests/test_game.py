from game import __version__
from game import game
from typing import List
import pytest
from unittest.mock import patch

# TODO: comment each test


@pytest.fixture
def example_empty_board() -> List[str]:
    board = [" ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
    return board


@pytest.fixture
def example_correct_123_board() -> List[str]:
    board = ["#", "X", "X", "X", "O", "X", "O", "X", "O", "O"]
    return board


@pytest.fixture
def example_correct_456_board() -> List[str]:
    board = ["#", "X", "O", "X", "O", "O", "O", "X", "O", "X"]
    return board


@pytest.fixture
def example_correct_789_board() -> List[str]:
    board = ["#", "X", "O", "X", "O", "O", "O", "X", "X", "X"]
    return board


@pytest.fixture
def example_correct_147_board() -> List[str]:
    board = ["#", "X", "O", "O", "X", "X", "O", "X", "O", "X"]
    return board


@pytest.fixture
def example_correct_258_board() -> List[str]:
    board = ["#", "X", "O", "X", "O", "X", "O", "X", "O", "X"]
    return board


@pytest.fixture
def example_correct_369_board() -> List[str]:
    board = ["#", "X", "O", "O", "X", "X", "O", "X", "O", "O"]
    return board


@pytest.fixture
def example_correct_159_board() -> List[str]:
    board = ["#", "X", "O", "O", "X", "X", "O", "X", "O", "X"]
    return board


@pytest.fixture
def example_correct_357_board() -> List[str]:
    board = ["#", "X", "O", "X", "O", "X", "O", "X", "O", "O"]
    return board


@pytest.fixture
def example_full_board() -> List[str]:
    board = [" ", "X", "O", "X", "O", "X", "O", "O", "X", "O"]
    return board


@pytest.fixture
def example_update_board() -> List[str]:
    board = [" ", "X", " ", " ", " ", " ", " ", " ", " ", " "]
    return board


def test_initial_empty_board(example_empty_board: List[str]):
    initial = game.Board()
    assert initial.cells == example_empty_board


def test_update_cell_empty_board(example_update_board: List[str]):
    board = game.Board()
    board.update_cell(1, "X")
    assert board.cells == example_update_board


def test_check_win_empty():
    board = game.Board()
    assert board.is_winner("X") == False


def test_win_check_123(example_correct_123_board: List[str]):
    board = game.Board()
    board.cells = example_correct_123_board
    assert board.is_winner("X") == True


def test_win_check_456(example_correct_456_board: List[str]):
    board = game.Board()
    board.cells = example_correct_456_board
    assert board.is_winner("O") == True


def test_win_check_789(example_correct_789_board: List[str]):
    board = game.Board()
    board.cells = example_correct_789_board
    assert board.is_winner("X") == True


def test_win_check_147(example_correct_147_board: List[str]):
    board = game.Board()
    board.cells = example_correct_147_board
    assert board.is_winner("X") == True


def test_win_check_258(example_correct_258_board: List[str]):
    board = game.Board()
    board.cells = example_correct_258_board
    assert board.is_winner("X") == True


def test_win_check_369(example_correct_369_board: List[str]):
    board = game.Board()
    board.cells = example_correct_369_board
    assert board.is_winner("X") == True


def test_win_check_159(example_correct_159_board: List[str]):
    board = game.Board()
    board.cells = example_correct_159_board
    assert board.is_winner("X") == True


def test_win_check_357(example_correct_357_board: List[str]):
    board = game.Board()
    board.cells = example_correct_357_board
    assert board.is_winner("X") == True


def test_is_full_true(example_full_board: List[str]):
    board = game.Board()
    board.cells = example_full_board
    assert board.is_full() == True


def test_is_full_emptyboard(example_empty_board: List[str]):
    empty = game.Board()
    assert empty.is_full() == False


def test_check_cell_valid_input():
    empty = game.Board()
    assert empty.check_cell(1, "X") == True


def test_check_cell_fullboard(example_full_board: List[str]):
    empty = game.Board()
    empty.cells = example_full_board
    assert empty.check_cell(1, "X") == False


def test_replay_game():
    board = game.Board()
    assert board.replay() == board.cells


def test_print_header(capfd):
    game.print_header()  # Writes "Hello World!" to stdout
    out, err = capfd.readouterr()
    assert out == "Welcome to Tic-Tac-Toe\n\n"
