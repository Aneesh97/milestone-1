from os import system
from typing import List

system("clear")


class Board:
    def __init__(self):
        """
        Initialises class Board with empty board as cells.
        """

        self.cells = [" ", " ", " ", " ", " ", " ", " ", " ", " ", " "]

        # TODO: self.move_no for checking draw

    def display(self):
        """
        Prints the board.
        """

        print(" %s | %s | %s" % (self.cells[1], self.cells[2], self.cells[3]))
        print("------------")
        print(" %s | %s | %s" % (self.cells[4], self.cells[5], self.cells[6]))
        print("------------")
        print(" %s | %s | %s" % (self.cells[7], self.cells[8], self.cells[9]))

    def check_cell(self, cell_no, player):
        is_valid = False

        cell_no = int(cell_no)
        if (cell_no > 0) and (cell_no < 10) and self.cells[cell_no] == " ":
            is_valid = True
            return is_valid
        elif self.cells[cell_no] != " ":
            print("That tile is already occupied!")
            is_valid = False
            return is_valid

    def update_cell(self, cell_no, player):
        """
        Updates cells on board with player's move.
        """
        if self.cells[cell_no] == " ":
            self.cells[cell_no] = player

    def is_winner(self, player) -> bool:
        """
        Checks if the game has been won

            Parameters:
                takes in Class Board and the current player

            Returns:
                bool value True for game won or False for game not won
        """

        for combo in [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
            [1, 4, 7],
            [2, 5, 8],
            [3, 6, 9],
            [1, 5, 9],
            [3, 5, 7],
        ]:
            result = True
            for cell_no in combo:
                if self.cells[cell_no] != player:
                    result = False
            if result == True:
                return True
        return False

    def is_full(self) -> bool:
        """
        Checks if board is full for determining draw. Returns True if full and False if not.
        """

        used_cells = 0
        for cell in self.cells:
            if cell != " ":
                used_cells += 1
        if used_cells == 9:
            return True
        else:
            return False

    def replay(self) -> List:
        """
        Restarts the game when called.
        """

        self.cells = [" ", " ", " ", " ", " ", " ", " ", " ", " ", " "]
        return self.cells


board = Board()


def print_header():
    print("Welcome to Tic-Tac-Toe\n")


def refresh_screen():
    """
    Refreshes the screen before every move and then prints current state of board.
    """

    # clear the screen
    system("clear")

    # print title
    print_header()

    # show the board
    board.display()


def main():
    while True:
        refresh_screen()

        # Get X input
        x_choice = int(input("\nX) Please choose 1 - 9. >"))

        # update board
        while True:
            if board.check_cell(x_choice, "X") == True:
                board.update_cell(x_choice, "X")
                break
            elif board.check_cell(x_choice, "X") == False:
                # refresh_screen()
                x_choice = int(input("\nX) Please choose 1 - 9. >"))
                continue  # pragma: no cover

        # refresh screen
        refresh_screen()

        # check for X winner
        if board.is_winner("X"):
            print("\nX wins!\n")
            play_again = input("Would you like to play again? (Y/N) > ").upper()
            if play_again == "Y":
                board.replay()
                continue  # pragma: no cover
            else:
                break

        # check for tie
        if board.is_full():
            print("\nTied game!\n")
            play_again = input("Would you like to play again? (Y/N) > ").upper()
            if play_again == "Y":
                board.replay()
                continue  # pragma: no cover
            else:
                break

        refresh_screen()

        # Get O input
        o_choice = int(input("\nO) Please choose 1 - 9. >"))

        # update board
        while True:
            if board.check_cell(o_choice, "O") == True:
                board.update_cell(o_choice, "O")
                break
            else:
                # refresh_screen()
                o_choice = int(input("\nO) Please choose 1 - 9. >"))
                continue  # pragma: no cover

        refresh_screen()

        # check for O winner
        if board.is_winner("O"):
            print("\nO wins!\n")
            play_again = input("Would you like to play again? (Y/N) > ").upper()
            if play_again == "Y":
                board.replay()
                continue  # pragma: no cover
            else:
                break

        # check for tie
        if board.is_full():
            print("\nTied game!\n")
            play_again = input("Would you like to play again? (Y/N) > ").upper()
            if play_again == "Y":
                board.replay()
                continue  # pragma: no cover
            else:
                break


if __name__ == "__main__":  # pragma: no cover
    main()
